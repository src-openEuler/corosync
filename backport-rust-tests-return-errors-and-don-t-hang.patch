From b98248d9a53726aaa13229f02d97c9b60246beb9 Mon Sep 17 00:00:00 2001
From: Christine Caulfield <ccaulfie@redhat.com>
Date: Mon, 8 Jan 2024 14:56:37 +0000
Subject: [PATCH] rust: tests return errors and don't hang

Signed-off-by: Christine Caulfield <ccaulfie@redhat.com>
Reviewed-by: Jan Friesse <jfriesse@redhat.com>
---
 bindings/rust/tests/src/bin/cfg-test.rs       | 22 ++++++-----
 bindings/rust/tests/src/bin/cmap-test.rs      | 39 ++++++++++---------
 bindings/rust/tests/src/bin/cpg-test.rs       | 24 +++++++-----
 bindings/rust/tests/src/bin/quorum-test.rs    | 21 +++++-----
 .../rust/tests/src/bin/votequorum-test.rs     | 23 ++++++-----
 5 files changed, 71 insertions(+), 58 deletions(-)

diff --git a/bindings/rust/tests/src/bin/cfg-test.rs b/bindings/rust/tests/src/bin/cfg-test.rs
index 1b245251..5b6f9ce0 100644
--- a/bindings/rust/tests/src/bin/cfg-test.rs
+++ b/bindings/rust/tests/src/bin/cfg-test.rs
@@ -23,7 +23,7 @@ fn shutdown_check_fn(handle: &cfg::Handle, _flags: u32) {
     }
 }
 
-fn main() {
+fn main() -> Result<(), corosync::CsError> {
     // Initialise the callbacks data
     let cb = cfg::Callbacks {
         corosync_cfg_shutdown_callback_fn: Some(shutdown_check_fn),
@@ -36,7 +36,7 @@ fn main() {
         }
         Err(e) => {
             println!("Error in CFG init: {e}");
-            return;
+            return Err(e);
         }
     };
 
@@ -48,7 +48,7 @@ fn main() {
         }
         Err(e) => {
             println!("Error in CFG init: {e}");
-            return;
+            return Err(e);
         }
     };
 
@@ -59,6 +59,7 @@ fn main() {
         }
         Err(e) => {
             println!("Error in CFG track_start: {e}");
+            return Err(e);
         }
     };
 
@@ -70,7 +71,7 @@ fn main() {
             }
             Err(e) => {
                 println!("Error in CFG local_get: {e}");
-                None
+                return Err(e);
             }
         }
     };
@@ -109,6 +110,7 @@ fn main() {
                 println!(
                     "Error in CFG node_status get: {e} (tried nodeids {us_plus1} & {us_less1})"
                 );
+                return Err(e);
             }
         }
     }
@@ -121,15 +123,15 @@ fn main() {
         Err(e) => {
             if e != corosync::CsError::CsErrBusy {
                 println!("Error in CFG try_shutdown: {e}");
+                return Err(e);
             }
         }
     }
 
-    // Wait for events
-    loop {
-        if cfg::dispatch(&handle, corosync::DispatchFlags::One).is_err() {
-            break;
-        }
+    // Quick test of dispatch
+    if let Err(e) = cfg::dispatch(&handle, corosync::DispatchFlags::OneNonblocking) {
+        println!("Error in CFG dispatch");
+        return Err(e);
     }
-    println!("ERROR: Corosync quit");
+    Ok(())
 }
diff --git a/bindings/rust/tests/src/bin/cmap-test.rs b/bindings/rust/tests/src/bin/cmap-test.rs
index 25ec722a..edc0305a 100644
--- a/bindings/rust/tests/src/bin/cmap-test.rs
+++ b/bindings/rust/tests/src/bin/cmap-test.rs
@@ -18,7 +18,7 @@ fn track_notify_fn(
     println!("   New value: {new_value}");
 }
 
-fn main() {
+fn main() -> Result<(), corosync::CsError> {
     let handle = match cmap::initialize(cmap::Map::Icmap) {
         Ok(h) => {
             println!("cmap initialized.");
@@ -26,48 +26,48 @@ fn main() {
         }
         Err(e) => {
             println!("Error in CMAP (Icmap) init: {e}");
-            return;
+            return Err(e);
         }
     };
 
     // Test some SETs
     if let Err(e) = cmap::set_u32(&handle, "test.test_uint32", 456) {
         println!("Error in CMAP set_u32: {e}");
-        return;
+        return Err(e);
     };
 
     if let Err(e) = cmap::set_i16(&handle, "test.test_int16", -789) {
         println!("Error in CMAP set_i16: {e}");
-        return;
+        return Err(e);
     };
 
     if let Err(e) = cmap::set_number(&handle, "test.test_num_1", 6809u32) {
         println!("Error in CMAP set_number(u32): {e}");
-        return;
+        return Err(e);
     };
 
     // NOT PI (just to avoid clippy whingeing)
     if let Err(e) = cmap::set_number(&handle, "test.test_num_2", 3.24159265) {
         println!("Error in CMAP set_number(f32): {e}");
-        return;
+        return Err(e);
     };
 
     if let Err(e) = cmap::set_string(&handle, "test.test_string", "Hello from Rust") {
         println!("Error in CMAP set_string: {e}");
-        return;
+        return Err(e);
     };
 
     let test_d = cmap::Data::UInt64(0xdeadbeefbacecafe);
     if let Err(e) = cmap::set(&handle, "test.test_data", &test_d) {
         println!("Error in CMAP set_data: {e}");
-        return;
+        return Err(e);
     };
 
     //    let test_d2 = cmap::Data::UInt32(6809);
     let test_d2 = cmap::Data::String("Test string in data 12345".to_string());
     if let Err(e) = cmap::set(&handle, "test.test_again", &test_d2) {
         println!("Error in CMAP set_data2: {e}");
-        return;
+        return Err(e);
     };
 
     // get them back again
@@ -78,7 +78,7 @@ fn main() {
 
         Err(e) => {
             println!("Error in CMAP get: {e}");
-            return;
+            return Err(e);
         }
     };
     match cmap::get(&handle, "test.test_int16") {
@@ -88,7 +88,7 @@ fn main() {
 
         Err(e) => {
             println!("Error in CMAP get: {e}");
-            return;
+            return Err(e);
         }
     };
 
@@ -99,7 +99,7 @@ fn main() {
 
         Err(e) => {
             println!("Error in CMAP get: {e}");
-            return;
+            return Err(e);
         }
     };
     match cmap::get(&handle, "test.test_num_2") {
@@ -109,7 +109,7 @@ fn main() {
 
         Err(e) => {
             println!("Error in CMAP get: {e}");
-            return;
+            return Err(e);
         }
     };
     match cmap::get(&handle, "test.test_string") {
@@ -119,7 +119,7 @@ fn main() {
 
         Err(e) => {
             println!("Error in CMAP get: {e}");
-            return;
+            return Err(e);
         }
     };
 
@@ -131,7 +131,7 @@ fn main() {
 
         Err(e) => {
             println!("Error in CMAP get: {e}");
-            return;
+            return Err(e);
         }
     };
 
@@ -151,7 +151,7 @@ fn main() {
     // Close this handle
     if let Err(e) = cmap::finalize(&handle) {
         println!("Error in CMAP get: {e}");
-        return;
+        return Err(e);
     };
 
     // Test notifications on the stats map
@@ -159,7 +159,7 @@ fn main() {
         Ok(h) => h,
         Err(e) => {
             println!("Error in CMAP (Stats) init: {e}");
-            return;
+            return Err(e);
         }
     };
 
@@ -176,11 +176,11 @@ fn main() {
         Ok(th) => th,
         Err(e) => {
             println!("Error in CMAP track_add {e}");
-            return;
+            return Err(e);
         }
     };
 
-    // Wait for events
+    // Wait for some events
     let mut event_num = 0;
     loop {
         if let Err(e) = cmap::dispatch(&handle, corosync::DispatchFlags::One) {
@@ -192,4 +192,5 @@ fn main() {
             break;
         }
     }
+    Ok(())
 }
diff --git a/bindings/rust/tests/src/bin/cpg-test.rs b/bindings/rust/tests/src/bin/cpg-test.rs
index c5a58edc..dd574940 100644
--- a/bindings/rust/tests/src/bin/cpg-test.rs
+++ b/bindings/rust/tests/src/bin/cpg-test.rs
@@ -49,7 +49,7 @@ fn totem_confchg_fn(_handle: &cpg::Handle, ring_id: cpg::RingId, member_list: Ve
     println!("  members: {member_list:?}");
 }
 
-fn main() {
+fn main() -> Result<(), corosync::CsError> {
     // Initialise the model data
     let md = cpg::ModelData::ModelV1(cpg::Model1Data {
         flags: cpg::Model1Flags::None,
@@ -62,13 +62,13 @@ fn main() {
         Ok(h) => h,
         Err(e) => {
             println!("Error in CPG init: {e}");
-            return;
+            return Err(e);
         }
     };
 
     if let Err(e) = cpg::join(&handle, "TEST") {
         println!("Error in CPG join: {e}");
-        return;
+        return Err(e);
     }
 
     match cpg::local_get(&handle) {
@@ -77,6 +77,7 @@ fn main() {
         }
         Err(e) => {
             println!("Error in CPG local_get: {e}");
+            return Err(e);
         }
     }
 
@@ -88,6 +89,7 @@ fn main() {
         }
         Err(e) => {
             println!("Error in CPG membership_get: {e}");
+            return Err(e);
         }
     }
 
@@ -95,7 +97,7 @@ fn main() {
     let set_context: u64 = 0xabcdbeefcafe;
     if let Err(e) = cpg::context_set(&handle, set_context) {
         println!("Error in CPG context_set: {e}");
-        return;
+        return Err(e);
     }
 
     // NOTE This will fail on 32 bit systems because void* is not u64
@@ -107,6 +109,7 @@ fn main() {
         }
         Err(e) => {
             println!("Error in CPG context_get: {e}");
+            return Err(e);
         }
     }
 
@@ -120,6 +123,7 @@ fn main() {
         }
         Err(e) => {
             println!("Error in CPG iter start: {e}");
+            return Err(e);
         }
     }
 
@@ -130,13 +134,13 @@ fn main() {
         &"This is a test".to_string().into_bytes(),
     ) {
         println!("Error in CPG mcast_joined: {e}");
+        return Err(e);
     }
 
-    // Wait for events
-    loop {
-        if cpg::dispatch(&handle, corosync::DispatchFlags::One).is_err() {
-            break;
-        }
+    // Quick test of dispatch
+    if let Err(e) = cpg::dispatch(&handle, corosync::DispatchFlags::OneNonblocking) {
+        println!("Error in CPG dispatch: {e}");
+        return Err(e);
     }
-    println!("ERROR: Corosync quit");
+    Ok(())
 }
diff --git a/bindings/rust/tests/src/bin/quorum-test.rs b/bindings/rust/tests/src/bin/quorum-test.rs
index 9436b392..ae7404ec 100644
--- a/bindings/rust/tests/src/bin/quorum-test.rs
+++ b/bindings/rust/tests/src/bin/quorum-test.rs
@@ -30,7 +30,7 @@ fn nodelist_fn(
     println!("  left: {left_list:?}");
 }
 
-fn main() {
+fn main() -> Result<(), corosync::CsError> {
     // Initialise the model data
     let md = quorum::ModelData::ModelV1(quorum::Model1Data {
         flags: quorum::Model1Flags::None,
@@ -45,7 +45,7 @@ fn main() {
         }
         Err(e) => {
             println!("Error in QUORUM init: {e}");
-            return;
+            return Err(e);
         }
     };
 
@@ -53,7 +53,7 @@ fn main() {
     let set_context: u64 = 0xabcdbeefcafe;
     if let Err(e) = quorum::context_set(&handle, set_context) {
         println!("Error in QUORUM context_set: {e}");
-        return;
+        return Err(e);
     }
 
     // NOTE This will fail on 32 bit systems because void* is not u64
@@ -61,23 +61,24 @@ fn main() {
         Ok(c) => {
             if c != set_context {
                 println!("Error: context_get() returned {c:x}, context should be {set_context:x}");
+                return Err(corosync::CsError::CsErrRustCompat);
             }
         }
         Err(e) => {
             println!("Error in QUORUM context_get: {e}");
+            return Err(e);
         }
     }
 
     if let Err(e) = quorum::trackstart(&handle, corosync::TrackFlags::Changes) {
         println!("Error in QUORUM trackstart: {e}");
-        return;
+        return Err(e);
     }
 
-    // Wait for events
-    loop {
-        if quorum::dispatch(&handle, corosync::DispatchFlags::One).is_err() {
-            break;
-        }
+    // Quick test of dispatch
+    if let Err(e) = quorum::dispatch(&handle, corosync::DispatchFlags::OneNonblocking) {
+        println!("Error in QUORUM dispatch: {e}");
+        return Err(e);
     }
-    println!("ERROR: Corosync quit");
+    Ok(())
 }
diff --git a/bindings/rust/tests/src/bin/votequorum-test.rs b/bindings/rust/tests/src/bin/votequorum-test.rs
index eebc9f0d..d37100a4 100644
--- a/bindings/rust/tests/src/bin/votequorum-test.rs
+++ b/bindings/rust/tests/src/bin/votequorum-test.rs
@@ -30,7 +30,7 @@ fn expectedvotes_fn(_handle: &votequorum::Handle, _context: u64, expected_votes:
     println!("TEST expected_votes_fn called: value is {expected_votes}");
 }
 
-fn main() {
+fn main() -> Result<(), corosync::CsError> {
     // Initialise the model data
     let cb = votequorum::Callbacks {
         quorum_notification_fn: Some(quorum_fn),
@@ -45,7 +45,7 @@ fn main() {
         }
         Err(e) => {
             println!("Error in VOTEQUORUM init: {e}");
-            return;
+            return Err(e);
         }
     };
 
@@ -53,6 +53,7 @@ fn main() {
     let set_context: u64 = 0xabcdbeefcafe;
     if let Err(e) = votequorum::context_set(&handle, set_context) {
         println!("Error in VOTEQUORUM context_set: {e}");
+        return Err(e);
     }
 
     // NOTE This will fail on 32 bit systems because void* is not u64
@@ -64,6 +65,7 @@ fn main() {
         }
         Err(e) => {
             println!("Error in VOTEQUORUM context_get: {e}");
+            return Err(e);
         }
     }
 
@@ -71,6 +73,7 @@ fn main() {
 
     if let Err(e) = votequorum::qdevice_register(&handle, QDEVICE_NAME) {
         println!("Error in VOTEQUORUM qdevice_register: {e}");
+        return Err(e);
     }
 
     match votequorum::get_info(&handle, corosync::NodeId::from(1u32)) {
@@ -91,27 +94,29 @@ fn main() {
                     "qdevice names do not match: s/b: \"{}\"  is: \"{}\"",
                     QDEVICE_NAME, i.qdevice_name
                 );
+                return Err(corosync::CsError::CsErrRustCompat);
             }
         }
         Err(e) => {
             println!("Error in VOTEQUORUM get_info: {e} (check nodeid 1 has been online)");
+            return Err(e);
         }
     }
 
     if let Err(e) = votequorum::qdevice_unregister(&handle, QDEVICE_NAME) {
         println!("Error in VOTEQUORUM qdevice_unregister: {e}");
+        return Err(e);
     }
 
     if let Err(e) = votequorum::trackstart(&handle, 99_u64, corosync::TrackFlags::Changes) {
         println!("Error in VOTEQUORUM trackstart: {e}");
-        return;
+        return Err(e);
     }
 
-    // Wait for events
-    loop {
-        if votequorum::dispatch(&handle, corosync::DispatchFlags::One).is_err() {
-            break;
-        }
+    // Quick test of dispatch
+    if let Err(e) = votequorum::dispatch(&handle, corosync::DispatchFlags::OneNonblocking) {
+        println!("Error in VOTEUORUM dispatch: {e}");
+        return Err(e);
     }
-    println!("ERROR: Corosync quit");
+    Ok(())
 }
-- 
2.25.1

